# COSC4397 Selected Topics: Project
This is a group project for COSC4397 Selected Topics at University of Houston.

Please add project constraints and design principles we should follow as needed.

This program is run using Docker and a local PHP server environment connecting with a local instance of MySQL.

## Setting up Docker image
1. Install [Docker](https://docs.docker.com/install/).
2. Install [composer](https://getcomposer.org/), [node](https://nodejs.org/en/), [PHP](http://php.net/downloads.php) and [MySQL](https://dev.mysql.com/downloads/) locally.
	- If you have a Mac or Linux, install using `brew` or `apt-get`.
3. If you're on Windows, be sure that the composer, node, PHP and MySQL binaries are in your system's environment variable path.
4. Run the following commands to start the MachineBox Docker image
    ```
    MB_KEY="MDY4ZTQ3MWE2OGQyYjgzZmNiOGZjYzAzYzYyNmZkMGE.MBTJqAtsDCJb3enqkhcH9lx48RQHgAWaxG0dAj6PnBUBthaaIcYiEzYzx6HHTNum8BMon5KauCUehetICQATmg"
    docker run -p 8080:8080 -e "MB_KEY=$MB_KEY" machinebox/facebox
    ```
5. Login in to your MySQL instance and run the following:
	```
	CREATE DATABASE face_lock;
	CREATE USER face_lock_user IDENTIFIED BY 'secret';
	GRANT ALL PRIVILEGES ON face_lock.* TO face_lock_user;
	FLUSH PRIVILEGES;
	exit;
	```
6. Run the following
	```
	cd /path/to/S18-T3/face-lock/demo/code/
	composer install
	npm install
	cd public
	php -S localhost:3000
	```

## Connecting PHPStorm with Our Database
1. Select View > Tool Windows > Database
2. A window should pop up on the right side. Click the Plus sign.
3. Select Data Source
4. Select MySQL
5. A new window should pop up.
6. Name: name it whatever you'd like that's specific to this project.
7. Host: localhost
8. Database: face_lock
9. User: face_lock_user
10. Password: secret
11. URL: leave alone
12. Driver: download if it's asking you to near the bottom
13. Click Test Connection.
14. Click Apply then Ok.

You should now be able to go to any SQL file within PHPStorm and press CTRL+Enter. You'll be prompted to select which statement you want to run.
