<?php

namespace App\Controllers;

use User;

class UsersController
{
    public function store()
    {
        $password = md5($_POST['password']);

        $web_file_path = '';

        //if($_FILES['image']) {
        //    $web_file_path = '/public/assets/images/face-lock/';
        //    $upload_dir = __DIR__ . '/../..' . $web_file_path;
        //    $file_name = basename($_FILES['image']['name']);
        //    $image_path = realpath($upload_dir . $file_name);
        //    $web_file_path .= $_FILES['image']['name'];
        //    move_uploaded_file($_FILES['image']['tmp_name'], $image_path);
        //}

        $userInsertId = User::insert(
            [
                'username'  => $_POST['username'],
                'email'   => $_POST['email'],
                'password'   => $password,
                'image_path' => $web_file_path
            ]
        );

        if (! \is_string($userInsertId)) {
            $user = User::find()
                        ->where('id', $userInsertId)
                        ->get();

            $_SESSION['user'] = serialize($user);

            return redirect('home');
        }

        return redirect('register');
    }
}