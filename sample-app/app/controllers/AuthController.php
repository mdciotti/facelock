<?php

namespace App\Controllers;

use State;
use User;

class AuthController
{
    public function login()
    {
        return view('auth/login');
    }

    public function register()
    {
        return view('auth/register');
    }

    public function faceLockLogin(){
        return view('auth/login-with-facelock');
    }

    public function faceLockAndPasswordLogin(){
        return view('auth/login-with-face-and-password');
    }

    /**
     * @return mixed direct user to their appropriate home page or redirect to login page with errors.
     */
    public function signIn()
    {
        $user = User::find()
                    ->where('username', $_POST['username'])
                    ->where('password', md5($_POST['password']))
                    ->get();
        //dd($user);
        if ($user) {
            $_SESSION['user'] = serialize($user);
            return redirect('home');
        }
        $_SESSION['errors'][] = "Username or password do not match.";

        return view('auth/login');
    }

    /**
     * @return mixed direct user to their appropriate home page or redirect to login page with errors.
     */
    public function faceLockSignIn()
    {
        if(isset($_POST['face-verify']) && !$_POST['face-verify'] ) {
            $_SESSION['errors'][] = "Username or face do not match.";
            return redirect('login-with-face-lock');
        }
        $errors = [];
        $user = User::find()
                    ->where('username', $_POST['username'])
                    ->get();
        //dd($user);
        if ($user) {
            $_SESSION['user'] = serialize($user);
            return redirect('home');
        }
        $_SESSION['errors'][] = "Username or face do not match.";

        return view('auth/login-with-facelock', compact('errors'));
    }

    /**
     * @return mixed direct user to their appropriate home page or redirect to login page with errors.
     */
    public function faceLockAndPasswordSignIn()
    {
        if(isset($_POST['face-verify']) && !$_POST['face-verify'] ) {
            $_SESSION['errors'][] = "Username, password or face do not match.";
            return redirect('login-with-face-and-password');
        }
        $errors = [];
        $user = User::find()
                    ->where('username', $_POST['username'])
                    ->where('password', md5($_POST['password']))
                    ->get();
        //dd($user);
        if ($user) {
            $_SESSION['user'] = serialize($user);
            return redirect('home');
        }
        $_SESSION['errors'][] = "Username, password or face do not match.";

        return view('auth/login-with-face-and-password', compact('errors'));
    }

    /**
     * @return mixed remove user's session data and redirect to the home page
     */
    public function logout()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        $_SESSION['user'] = false;

        return redirect('');
    }
}