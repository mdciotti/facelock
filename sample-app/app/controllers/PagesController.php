<?php

namespace App\Controllers;

class PagesController
{
    /**
     * @return mixed Display static home page
     */
    public function index()
    {
        return view('index');
    }

    /**
     * @return mixed Display static Errors page
     */
    public function error()
    {
        return view('error/404');
    }
}