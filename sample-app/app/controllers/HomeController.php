<?php

namespace App\Controllers;

use Address;
use App\Core\Auth;
use Package;
use PackageStatus;
use State;
use Transaction;
use User;

class HomeController
{
    /**
     * @return mixed returns dashboard view for the currently signed in Employee
     */
    public function home()
    {
        if(!($user = Auth::user())) {
            return redirect('login');
        }


        return view('home/account', compact('user'));
    }
}