<?php
/**
 * Basic stub for storing database entities in their appropriate class
 */

use App\Models\Model;

class User extends Model
{
    public function __construct()
    {
        // silence is golden
    }
}