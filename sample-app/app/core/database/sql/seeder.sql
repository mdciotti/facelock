# noinspection SqlNoDataSourceInspectionForFile

/**
	Basic schema for storing Users
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  id         INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  username   VARCHAR(50) UNIQUE,
  email      VARCHAR(100) UNIQUE,
  image_path VARCHAR(255) UNIQUE NULL,
  password   VARCHAR(32)
);