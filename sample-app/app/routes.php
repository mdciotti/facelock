<?php

/**
 * Declare all routes here.
 */
$router->get('', 'AuthController@login');
$router->get('home', 'HomeController@home');
$router->post('face-lock', 'AuthController@verifyFace');

/**
 * Authentication routes
 */
$router->get('register', 'AuthController@register');
$router->post('register', 'UsersController@store');
$router->get('login', 'AuthController@login');
$router->post('login', 'AuthController@signIn');
$router->get('login-with-face-lock', 'AuthController@faceLockLogin');
$router->post('login-with-face-lock', 'AuthController@faceLockSignIn');
$router->get('login-with-face-and-password', 'AuthController@faceLockAndPasswordLogin');
$router->post('login-with-face-and-password', 'AuthController@faceLockAndPasswordSignIn');
$router->get('logout', 'AuthController@logout');

/**
 * Error out
 */
$router->get('404', 'PagesController@error');