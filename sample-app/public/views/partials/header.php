<?php
	use App\Core\Auth;
	$user = Auth::user();
?>
<!doctype html>
<html>
	<head>
		<title>FaceLock</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/assets/bundle/css/all.min.css">
	</head>

	<body class="card">
		<?php getPartial( 'nav' ) ?>
		<div id="ContentWrapper">
			<div class="row nav-menu card clearfix">
				<header class="card primary-bg">
					<button class="nav-trigger float-right primary-bg"><span>toggle</span></button>
					<div class="container clearfix">
						<div class="float-left">
                            <a href="/" id="Logo">
                                <h1>
                                    FaceLock
                                </h1>
                            </a>
						</div>

					</div>
				</header>
			</div>
			<div class="row">
				<div id="Content" class="">