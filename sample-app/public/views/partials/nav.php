<?php

	use App\Core\Auth;

?>
<nav class="nav-item">
	<ul class="clearfix menu navigation">
		<li class="parent-menu nav-item"><a href="/">FaceLock</a></li>
		<?php if( empty( Auth::user() ) ) { ?>
            <li class="parent-menu nav-item"><a href="/login">Login</a></li>
            <li class="parent-menu nav-item"><a href="/login-with-face-lock">Login with FaceLock</a></li>
            <li class="parent-menu nav-item"><a href="/login-with-face-and-password">Login with FaceLock<br/>and Password</a></li>
			<li class="parent-menu nav-item"><a href="/register">Register</a></li>
		<?php } else { ?>
            <li class="parent-menu nav-item"><a href="/home">Home</a></li>
			<li class="parent-menu nav-item"><a href="/logout">Log Out</a></li>
		<?php } ?>
	</ul>
</nav>