				</div>
				<!-- /.container -->
				<div id="Footer" class="row primary-bg">
					<div class="container">
						FaceLock 2018&copy;
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /#ContentWrapper -->
        <script src="/assets/bundle/js/all.min.js"></script>

        <script>
            const fv = new machinebox.FaceVerify({
                faceBox: 'http://localhost:8080',
                videoSelector: '#facePreview',
                snapshotInterval: 1000,
                error: function(error) {
                    let inputError = $('.ui.error.message');
                    if (!error) {
                        inputError.hide();
                        return;
                    }
                    inputError.text(error).show();
                },
                onSecure: function(name) {
                    $('.status').text('Hello ' + name);
                    $('.secure').css({backgroundColor: 'white'});
                    if(name === $('#username').val()) {
                        $('#faceVerifyFlag').attr('value', 1);
                        if($('#loginWithFaceLock').val() === '1') {
                        	$('#loginForm').submit();
                        }
                    }
                },
                onInsecure: function(message) {
                    $('.status').text(message);
                    $('.secure').css({backgroundColor: 'black'});
                    if(name === $('#username').val()) {
                        $('#faceVerifyFlag').attr('value', 0);
                    }
                },
            });
            fv.start()
        </script>
	</body>
</html>