<?php getHeader(); ?>

    <div class="row">
        <div class="container">
            <div class="col-dt-8">
                <div class="form-wrapper">
                    <h2>Sign In!</h2>
                    <form id="loginForm" action="/login-with-face-lock" method="post">
                        <div class="field-container validate required">
                            <label for="username">Username <span>*</span></label>
                            <input id="username" type="text" name="username" value="adrian" required class="valid">
                        </div>
                        <!-- /.field-container -->
                        <input id="faceVerifyFlag" type="hidden" name="face-verify" value="0" required>
                        <input id="loginWithFaceLock" type="hidden" name="loginWithFaceLock" value="1" required>
                        <?php if (!empty($errors)) { ?>
                            <div class="errors">
                                <?php foreach ($errors as $error) { ?>
                                    <?= $error; ?>
                                    <!-- /.error -->
                                <?php } ?>
                            </div>
                        <?php } ?>
<!--                        <button type="submit">Submit</button>-->
                    </form>
                </div>
                <!-- /.form-wrapper -->
            </div>
            <!-- /.col-dt-8 -->
            <div class="col col-4 px2">
                <?php getPartial('video-canvas'); ?>
            </div>
            <!-- /.col-dt-4 -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.row -->

<?php getFooter(); ?>