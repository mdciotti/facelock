<?php
	getHeader();
	$emailClass = (isset($errors['email']))?'invalid':'';
?>

	<div class="row">
		<div class="container">
			<div class="form-wrapper">
				<h2>Sign up!</h2>
				<form id="registrationForm" action="/register" method="post" enctype="multipart/form-data">
					<div class="field-container clearfix required">
						<label for="username" class="required">Username <span>*</span></label>
						<input id="username" type="text" name="username" class="valid" value="adrian" required>
					</div>
                    <!-- /.field-container -->
                    <div class="field-container validate clearfix required">
                        <label for="email" class="email required">Email <span>*</span></label>
                        <input id="email" type="email" name="email" value="adrian@uh.edu" class="valid <?= $emailClass; ?>" required>
                    </div>
                    <!-- /.field-container -->
                    <div class="field-container clearfix required">
                        <label for="password" class="required">Password <span>*</span></label>
                        <input id="password" type="password" name="password" value="secret" required class="valid">
                    </div>
                    <!-- /.field-container -->
                    <div class="field-container clearfix">
                        <label for="image" class="filled">Image Verification</label>
                        <input id="image" type="file" name="image" value=""><br/>
                        <small>Provide an image of your face looking head-on at the camera</small>
                    </div>
                    <!-- /.field-container -->
					<button id="submit" type="submit">Submit</button>
				</form>
			</div>
			<!-- /.form-wrapper -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.row -->

<?php getFooter(); ?>