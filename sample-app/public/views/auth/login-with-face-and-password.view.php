<?php getHeader(); ?>

    <div class="row">
        <div class="container">
            <div class="col-dt-8">
                <div class="form-wrapper">
                    <h2>Sign In!</h2>
                    <form id="loginForm" action="/login-with-face-and-password" method="post">
                        <div class="field-container validate required">
                            <div class="field-container validate required">
                                <label for="username">Username <span>*</span></label>
                                <input id="username" type="text" name="username" value="adrian" required class="valid">
                            </div>
                            <!-- /.field-container -->
                            <!--						<input type="email" name="email" required>-->
                        </div>
                        <!-- /.field-wrapper -->
                        <div class="field-container required">
                            <label for="password">Password <span>*</span></label>
                            <input id="password" type="password" name="password" value="secret" required class="valid">
                        </div>
                        <!-- /.field-wrapper -->
                        <div class="field-container required">
                            <input id="faceVerifyFlag" type="hidden" name="face-verify" value="0" required>
                        </div>
                        <!-- /.field-wrapper -->
                        <?php if (!empty($errors)) { ?>
                            <div class="errors">
                                <?php foreach ($errors as $error) { ?>
                                    <?= $error; ?>
                                    <!-- /.error -->
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <button type="submit">Submit</button>
                    </form>
                </div>
                <!-- /.form-wrapper -->
            </div>
            <!-- /.col-dt-8 -->
            <div class="col col-4 px2">
                <?php getPartial('video-canvas'); ?>
            </div>
            <!-- /.col-dt-4 -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.row -->

<?php getFooter(); ?>