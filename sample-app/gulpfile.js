'use strict';

/**
 * Used for concatenating, linting, and minifying all JS and CSS files.
 * @type {Gulp}
 */

var gulp = require('gulp');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concatCss = require('gulp-concat-css');
var cleanCss = require('gulp-clean-css');

var JS_SRC = [
	'public/assets/js/vendor/*.js',
	'public/assets/js/face-verify.js',
	'public/assets/js/main.js',
];

gulp.task('default', ['css', 'scripts', 'watch']);

gulp.task('css', function() {
	return gulp.src('public/assets/css/*.css')
						 .pipe(concatCss('all.min.css'))
						 .pipe(cleanCss())
						 .pipe(gulp.dest('public/assets/bundle/css'));
});

gulp.task('scripts', function() {
	return gulp.src(JS_SRC)
						 .pipe(concat('all.min.js'))
						 .pipe(gulp.dest('public/assets/bundle/js'))
						 .pipe(rename('all.min.js'))
						 // .pipe(uglify())
						 .pipe(gulp.dest('public/assets/bundle/js'));
});

gulp.task('watch', function() {
	gulp.watch(JS_SRC, ['scripts']);
	gulp.watch('public/assets/css/*.css', ['css']);
});