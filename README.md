FaceLock
========

> Login form using facial authentication from MachineBox

A live demo is available at https://facelock.mdc.io


About
-----

FaceLock is a authentication provider that enables website users to register and sign in using facial recognition. Installation is simple:

1. Add the FaceLock button to your website
2. Enjoy logging in without a password!


Demo Setup
----------

To run the demo locally, serve the `facelock-button` directory using your favorite static HTTP server. If you have NodeJS installed, you can run

```sh
npm install -g serve
serve facelock-button
```

If you don't have NodeJS, you can use Python:

```sh
cd facelock-button
python -m "SimpleHTTPServer"
```


System Architecture
-------------------

![](https://github.com/cosc4397/S18-T3/blob/master/facial_login_design.png)

FaceLock is powered by [Facebox](https://machinebox.io/docs/facebox) from MachineBox. This is a central server which handles the facial recognition and training. It is available as a Docker container `machinebox/facebox`, which is hosted at https://facebox.mdc.io through OpenShift. 

A sample app is provided to demonstrate the usage of FaceLock. It is written as a PHP backend, and the source is contained in `sample-app`.

The FaceLock interface is provided as a login button which opens the authentication panel. It is written in JavaScript using the [Vue](https://vuejs.org/) framework, and the source code is located in `facelock-button`.


Implementation Notes
--------------------

As this is a proof of concept, it is not intended to be a reference implementation. There are many security considerations which were overlooked for the ease of demonstration given the short time frame of development.
